package no.noroff.accelerate.characters;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

// Character tests - see method test names for what they are testing
class CharacterTest {

    @Test
    void Character_WhenCreated_IsLevelOne() {
        Character character = new Warrior("Rythern");

        int expected = 1;

        int actual = character.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void LevelUp_GainsOneLevelAtLevelOne_IsLevelTwo() {
        Character character = new Warrior("Rythern");
        character.levelUp();

        int expected = 2;

        int actual = character.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void CharacterMage_CorrectDefaultAttributesWhenCreated_OneStrengthOneDexterityEightIntelligence() {
        Mage mage = new Mage("MagicalMike");

        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;

        int actualStrength = mage.basePrimaryAttributes.getStrength();
        int actualDexterity = mage.basePrimaryAttributes.getDexterity();
        int actualIntelligence = mage.basePrimaryAttributes.getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void CharacterRanger_CorrectDefaultAttributesWhenCreated_OneStrengthSevenDexterityOneIntelligence() {
        Ranger ranger = new Ranger("RangerRoger");

        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;

        int actualStrength = ranger.basePrimaryAttributes.getStrength();
        int actualDexterity = ranger.basePrimaryAttributes.getDexterity();
        int actualIntelligence = ranger.basePrimaryAttributes.getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void CharacterRogue_CorrectDefaultAttributesWhenCreated_TwoStrengthSixDexterityOneIntelligence() {
        Rogue rogue = new Rogue("StealthySteve");

        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;

        int actualStrength = rogue.basePrimaryAttributes.getStrength();
        int actualDexterity = rogue.basePrimaryAttributes.getDexterity();
        int actualIntelligence = rogue.basePrimaryAttributes.getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void CharacterWarrior_CorrectDefaultAttributesWhenCreated_FiveStrengthTwoDexterityOneIntelligence() {
        Warrior warrior = new Warrior("BruisingBruce");

        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;

        int actualStrength = warrior.basePrimaryAttributes.getStrength();
        int actualDexterity = warrior.basePrimaryAttributes.getDexterity();
        int actualIntelligence = warrior.basePrimaryAttributes.getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void LevelUp_CorrectDefaultAttributesWhenLevelOneMageLevelsUp_TwoStrengthTwoDexterityThirteenIntelligence() {
        Mage mage = new Mage("MagicalMike");
        mage.levelUp();

        int expectedStrength = 2;
        int expectedDexterity = 2;
        int expectedIntelligence = 13;

        int actualStrength = mage.basePrimaryAttributes.getStrength();
        int actualDexterity = mage.basePrimaryAttributes.getDexterity();
        int actualIntelligence = mage.basePrimaryAttributes.getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void LevelUp_CorrectDefaultAttributesWhenLevelOneRangerLevelsUp_TwoStrengthTwelveDexterityTwoIntelligence() {
        Ranger ranger = new Ranger("RangerRoger");
        ranger.levelUp();

        int expectedStrength = 2;
        int expectedDexterity = 12;
        int expectedIntelligence = 2;

        int actualStrength = ranger.basePrimaryAttributes.getStrength();
        int actualDexterity = ranger.basePrimaryAttributes.getDexterity();
        int actualIntelligence = ranger.basePrimaryAttributes.getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void LevelUp_CorrectDefaultAttributesWhenLevelOneRogueLevelsUp_ThreeStrengthTenDexterityTwoIntelligence() {
        Rogue rogue = new Rogue("StealthySteve");
        rogue.levelUp();

        int expectedStrength = 3;
        int expectedDexterity = 10;
        int expectedIntelligence = 2;

        int actualStrength = rogue.basePrimaryAttributes.getStrength();
        int actualDexterity = rogue.basePrimaryAttributes.getDexterity();
        int actualIntelligence = rogue.basePrimaryAttributes.getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void LevelUp_CorrectDefaultAttributesWhenLevelOneWarriorLevelsUp_EightStrengthFourDexterityTwoIntelligence() {
        Warrior warrior = new Warrior("BruisingBruce");
        warrior.levelUp();

        int expectedStrength = 8;
        int expectedDexterity = 4;
        int expectedIntelligence = 2;

        int actualStrength = warrior.basePrimaryAttributes.getStrength();
        int actualDexterity = warrior.basePrimaryAttributes.getDexterity();
        int actualIntelligence = warrior.basePrimaryAttributes.getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }
}