package no.noroff.accelerate.items;

import no.noroff.accelerate.characters.PrimaryAttribute;
import no.noroff.accelerate.characters.Warrior;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

// Item tests - see method test names for what they are testing
class ItemTest {

    @Test
    void EquipWeapon_EquipWeaponWithTooHighLevel_InvalidWeaponExceptionThrown() {
        Warrior warrior = new Warrior("Rythern");
        Weapon weapon = new Weapon("Common Axe", 2, Slot.WEAPON, WeaponType.AXE, 7, 1.1);

        String expected = "Your level is too low to equip this weapon";
        String actual = "";

        try {
            warrior.equipWeapon(weapon);
            fail("My method didn't throw when I expected it to");
        } catch (Exception expectedException) {
            actual = expectedException.getMessage();
        }

        assertEquals(expected, actual);
    }

    @Test
    void EquipArmor_EquipArmorWithTooHighLevel_InvalidArmorExceptionThrown() {
        Warrior warrior = new Warrior("Rythern");
        Armor armor = new Armor("Common Plate Body Armor", 2, Slot.BODY, ArmorType.PLATE, new PrimaryAttribute(1,0,0));

        String expected = "Your level is too low to equip this armor piece";
        String actual = "";

        try {
            warrior.equipArmor(armor);
            fail("My method didn't throw when I expected it to");
        } catch (Exception expectedException) {
            actual = expectedException.getMessage();
        }

        assertEquals(expected, actual);
    }

    @Test
    void EquipWeapon_EquipWrongWeaponType_InvalidWeaponExceptionThrown() {
        Warrior warrior = new Warrior("Rythern");
        Weapon weapon = new Weapon("Common Bow", 1, Slot.WEAPON, WeaponType.BOW, 12, 0.8);

        String expected = "The weapon can't be equipped by a Warrior";
        String actual = "";

        try {
            warrior.equipWeapon(weapon);
            fail("My method didn't throw when I expected it to");
        } catch (Exception expectedException) {
            actual = expectedException.getMessage();
        }

        assertEquals(expected, actual);
    }

    @Test
    void EquipArmor_EquipWrongArmorType_InvalidArmorExceptionThrown() {
        Warrior warrior = new Warrior("Rythern");
        Armor armor = new Armor("Common Cloth Head Armor", 1, Slot.HEAD, ArmorType.CLOTH, new PrimaryAttribute(0,0,5));

        String expected = "The armor piece can't be equipped by a Warrior";
        String actual = "";

        try {
            warrior.equipArmor(armor);
            fail("My method didn't throw when I expected it to");
        } catch (Exception expectedException) {
            actual = expectedException.getMessage();
        }

        assertEquals(expected, actual);
    }

    @Test
    void EquipWeapon_EquipValidWeapon_returnBooleanTrue() throws InvalidWeaponException {
        Warrior warrior = new Warrior("Rythern");
        Weapon weapon = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);

        boolean expected = true;
        boolean actual = warrior.equipWeapon(weapon);

        assertEquals(expected, actual);
    }

    @Test
    void EquipArmor_EquipValidArmor_ShouldReturnBooleanTrue() throws InvalidArmorException {
        Warrior warrior = new Warrior("Rythern");
        Armor armor = new Armor("Common Plate Body Armor", 1, Slot.BODY, ArmorType.PLATE, new PrimaryAttribute(1,0,0));

        boolean expected = true;
        boolean actual = warrior.equipArmor(armor);

        assertEquals(expected, actual);
    }

    @Test
    void CalculateDPS_NoWeaponEquipped_ShouldReturnOnePointZeroFive() {
        Warrior warrior = new Warrior("Rythern");

        double expected = 1.0*(1.0 + (5.0/100.0));

        double actual = warrior.calculateDPS();

        assertEquals(expected, actual);
    }

    @Test
    void CalculateDPS_ValidWeaponEquipped_ShouldReturnOnePointZeroFive() throws InvalidWeaponException {
        Warrior warrior = new Warrior("Rythern");
        Weapon weapon = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        warrior.equipWeapon(weapon);

        double expected = (7.0*1.1)*(1.0 + (5.0/100.0));

        double actual = warrior.calculateDPS();

        assertEquals(expected, actual);
    }

    @Test
    void CalculateDPS_ValidWeaponAndArmorEquipped_ShouldReturnOnePointZeroFive() throws InvalidWeaponException, InvalidArmorException {
        Warrior warrior = new Warrior("Rythern");
        Weapon weapon = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        Armor armor = new Armor("Common Plate Body Armor", 1, Slot.BODY, ArmorType.PLATE, new PrimaryAttribute(1,0,0));
        warrior.equipWeapon(weapon);
        warrior.equipArmor(armor);

        double expected = (7.0*1.1)*(1.0 + ((5.0 + 1.0)/100.0));

        double actual = warrior.calculateDPS();

        assertEquals(expected, actual);
    }
}