package no.noroff.accelerate.utils;

import no.noroff.accelerate.characters.Character;
import no.noroff.accelerate.items.ArmorType;
import no.noroff.accelerate.items.Slot;
import no.noroff.accelerate.items.Weapon;
import no.noroff.accelerate.items.WeaponType;

import java.util.Arrays;
import java.util.Scanner;

public class ConsoleLogic {
    static Scanner scanner = new Scanner(System.in);

    // Method that reads user input from console using choices with numbers
    public static int readInt (String prompt, int userChoices) {
        int input;

        do {
            System.out.println(prompt);
            try {
                input = Integer.parseInt(scanner.next());
            } catch (Exception e) {
                input = -1;
                System.out.println("Please enter an integer!");
            }
        } while (input < 1 || input > userChoices);

        return input;
    }

    // Method that prints separator with length n
    public static void printSeparator(int n) {
        StringBuilder separator = new StringBuilder();
        for (int i = 0; i < n; i++) {
            separator.append("-");
        }
        System.out.println(separator);
    }

    // Method that prints a heading for the console
    public static void printHeading (String title) {
        printSeparator(30);
        System.out.println(title);
        printSeparator(30);
    }

    // Method that starts the character creation and sets the name of the character
    public static String startCreation() {
        boolean nameSet = false;
        String name;

        // Print title start screen
        printSeparator(30);
        System.out.println("Welcome to my RPG character creation application!");

        // Getting the name from input
        do {
            printHeading("What's your character name?");
            System.out.println("->");
            name = scanner.next();

            // Asks if the user is satisfied with his choice
            printHeading("Your character name is " + name + "\nAre you satisfied with your name?");
            System.out.println("(1) Yes!");
            System.out.println("(2) No, I want to change my name.");
            int input = readInt("->", 2);
            if (input == 1) {
                nameSet = true;
            }
        } while (!nameSet);

        return name;
    }

    //  Method that allows the user to choose a character class
    public static String chooseCharacterClass (String name) {
        String characterClass = "";

        printHeading(name + ", what class do you want to be?");
        System.out.println("(1) Mage | (2) Ranger | (3) Rogue | (4) Warrior");

        int input = readInt("->", 4);

        // Sets character class based on choice
        if (input == 1) {
            characterClass = "Mage";
        } else if (input == 2) {
            characterClass = "Ranger";
        } else if (input == 3) {
            characterClass = "Rogue";
        } else if (input == 4) {
            characterClass = "Warrior";
        } else {
            System.out.println("Please choose a class!");
        }

        return characterClass;
    }

    // Method that levels the character up
    public static int levelUp (Character character) {
        int numberOfLevels;

        printHeading("You have chosen to level up\nYour " + character.getClass().getSimpleName() + " is level: " + character.getLevel());
        System.out.println("How many times do you wish to level up?");
        numberOfLevels = scanner.nextInt();

        return numberOfLevels;
    }

    // Method that allows the user to choose slot for armor or weapon
    public static Slot chooseSlot() {
        Slot slot = null;

        printHeading("What item type do you want to equip?");
        Arrays.stream(Slot.values())
                .forEach(s -> System.out.print("(" + (s.ordinal() + 1) + ") " +
                        (s.name().charAt(0) + s.name().substring(1).toLowerCase()) +
                        (s.ordinal() + 1 == Slot.values().length ? "\n":" |")));
        int input = readInt("->", 4);
        slot = Slot.values()[input - 1];

        return slot;
    }

    // Method that allows the user to choose a weapon type
    public static WeaponType chooseWeaponType() {
        WeaponType weapon = null;

        printHeading("What weapon do you want to equip?");
        Arrays.stream(WeaponType.values())
                .forEach(w -> System.out.print("(" + (w.ordinal() + 1) + ") " +
                        (w.name().charAt(0) + w.name().substring(1).toLowerCase()) +
                        (w.ordinal() + 1 == WeaponType.values().length ? "\n":" |")));
        int input = readInt("->", 7);
        weapon = WeaponType.values()[input - 1];

        return weapon;
    }

    // Method that allows the user to choose an armor type
    public static ArmorType chooseArmorType() {
        ArmorType armor = null;

        printHeading("What armor piece do you want to equip?");
        Arrays.stream(ArmorType.values())
                .forEach(w -> System.out.print("(" + (w.ordinal() + 1) + ") " +
                        (w.name().charAt(0) + w.name().substring(1).toLowerCase()) +
                        (w.ordinal() + 1 == ArmorType.values().length ? "\n":" |")));
        int input = readInt("->", 7);
        armor = ArmorType.values()[input - 1];

        return armor;
    }

    // Method that allows user to either choose to level up, browse items or show character sheet
    public static String chooseAction () {
        String choice = "";

        printHeading("What do you want to do now?");
        System.out.println("(1) Level up | (2) Browse Items | (3) Display character sheet");
        int input = readInt("->", 3);

        // Sets action based on choice
        if (input == 1) {
            choice = "level";
        } else if (input == 2) {
            choice = "browse";
        } else if (input == 3) {
            choice = "sheet";
        } else {
            System.out.println("Please choose an action!");
        }

        return choice;
    }
}
