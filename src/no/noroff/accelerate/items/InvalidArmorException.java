package no.noroff.accelerate.items;

import java.io.IOException;

// Custom IOException that should be thrown when invalid armor is equipped
public class InvalidArmorException extends IOException {
    public InvalidArmorException(String message) {
        super(message);
    }
}
