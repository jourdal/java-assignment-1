package no.noroff.accelerate.items;

// Enum for the different types of armor pieces
public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
