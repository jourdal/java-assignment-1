package no.noroff.accelerate.items;

import java.io.IOException;

// Custom IOException that should be thrown when invalid weapon is equipped
public class InvalidWeaponException extends IOException {
    public InvalidWeaponException(String message) {
        super(message);
    }
}
