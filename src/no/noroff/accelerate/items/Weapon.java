package no.noroff.accelerate.items;

public class Weapon extends Item {
    // Weapon properties
    private WeaponType weaponType;
    private int damage;
    private double attacksPerSecond;

    // Constructor for weapon
    public Weapon(String name, int requiredLevel, Slot slot, WeaponType weaponType, int damage, double attacksPerSecond) {
        super(name, requiredLevel, slot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attacksPerSecond = attacksPerSecond;
    }

    // Method that calculates weapon DPS
    public double weaponDPS () {
        return damage * attacksPerSecond;
    }

    // Getters for weapon
    public WeaponType getWeaponType() {
        return weaponType;
    }
}
