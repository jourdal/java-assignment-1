package no.noroff.accelerate.items;

public abstract class Item {
    // Item properties
    protected String name;
    protected int requiredLevel;
    protected Slot slot;

    // Item constructor
    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    // Getters for item
    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }
}
