package no.noroff.accelerate.items;

// Enum for the different slots of items
public enum Slot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
