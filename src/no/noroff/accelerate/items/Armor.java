package no.noroff.accelerate.items;

import no.noroff.accelerate.characters.PrimaryAttribute;

public class Armor extends Item {
    // Armor properties
    private ArmorType armorType;
    private PrimaryAttribute armorAttributes;

    // Armor constructor
    public Armor(String name, int requiredLevel, Slot slot, ArmorType armorType, PrimaryAttribute armorAttributes) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.armorAttributes = armorAttributes;
    }

    // Getters
    public ArmorType getArmorType() {
        return armorType;
    }

    public PrimaryAttribute getArmorAttributes() {
        return armorAttributes;
    }
}
