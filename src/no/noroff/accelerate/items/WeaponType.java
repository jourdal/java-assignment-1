package no.noroff.accelerate.items;

// Enum for the different types of weapon
public enum WeaponType {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND
}
