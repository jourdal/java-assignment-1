package no.noroff.accelerate.characters;

import static no.noroff.accelerate.items.ArmorType.*;
import static no.noroff.accelerate.items.WeaponType.*;
import no.noroff.accelerate.items.ArmorType;
import no.noroff.accelerate.items.WeaponType;

public class Warrior extends Character {

    // Constructor for warrior
    public Warrior(String name) {
        super(name, new PrimaryAttribute(5,2,1),
                new ItemType(new WeaponType[]{AXE, HAMMER, SWORD}, new ArmorType[]{MAIL, PLATE}));
    }

    // Method that increases level and base/total stats upon level up
    @Override
    public void levelUp() {
        level++;
        basePrimaryAttributes.increaseStrength(3);
        basePrimaryAttributes.increaseDexterity(2);
        basePrimaryAttributes.increaseIntelligence(1);
        totalPrimaryAttributes.increaseStrength(3);
        totalPrimaryAttributes.increaseDexterity(2);
        totalPrimaryAttributes.increaseIntelligence(1);
    }

    // Method that calculates DPS based on equipped weapon (if there is one) and total main stat
    @Override
    public double calculateDPS() {
        return weapon != null ? weapon.weaponDPS() * (1.0 + totalPrimaryAttributes.getStrength() / 100.0):
                (1.0 + totalPrimaryAttributes.getStrength() / 100.0);
    }
}
