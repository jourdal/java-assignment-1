package no.noroff.accelerate.characters;

public class PrimaryAttribute {
    // Properties for primary attribute
    private int strength;
    private int dexterity;
    private int intelligence;

    // Constructor for primary attribute
    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // Methods to set each of the attributes
    public void increaseStrength (int strength) {
        this.strength += strength;
    }

    public void increaseDexterity (int dexterity) {
        this.dexterity += dexterity;
    }

    public void increaseIntelligence (int intelligence) {
        this.intelligence += intelligence;
    }

    // Getters for each of the attributes
    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }
}
