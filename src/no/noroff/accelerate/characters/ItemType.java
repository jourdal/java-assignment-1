package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.ArmorType;;
import no.noroff.accelerate.items.WeaponType;

public class ItemType {
    // Itemtype are separated into weapontypes and armortypes
    private WeaponType[] weaponTypes;
    private ArmorType[] armorTypes;

    // Constructor for itemtype
    public ItemType(WeaponType[] weaponTypes, ArmorType[] armorTypes) {
        this.weaponTypes = weaponTypes;
        this.armorTypes = armorTypes;
    }

    // Getters for itemtype
    public WeaponType[] getWeaponTypes() {
        return weaponTypes;
    }

    public ArmorType[] getArmorTypes() {
        return armorTypes;
    }
}
