package no.noroff.accelerate.characters;

import static no.noroff.accelerate.items.ArmorType.*;
import static no.noroff.accelerate.items.WeaponType.*;
import no.noroff.accelerate.items.ArmorType;
import no.noroff.accelerate.items.WeaponType;

public class Rogue extends Character {

    // Constructor for rogue
    public Rogue(String name) {
        super(name, new PrimaryAttribute(2,6,1),
                new ItemType(new WeaponType[]{DAGGER, SWORD}, new ArmorType[]{LEATHER, MAIL}));
    }

    // Method that increases level and base/total stats upon level up
    @Override
    public void levelUp() {
        level++;
        basePrimaryAttributes.increaseStrength(1);
        basePrimaryAttributes.increaseDexterity(4);
        basePrimaryAttributes.increaseIntelligence(1);
        totalPrimaryAttributes.increaseStrength(1);
        totalPrimaryAttributes.increaseDexterity(4);
        totalPrimaryAttributes.increaseIntelligence(1);
    }

    // Method that calculates DPS based on equipped weapon (if there is one) and total main stat
    @Override
    public double calculateDPS() {
        return weapon != null ? weapon.weaponDPS() * (1.0 + totalPrimaryAttributes.getDexterity() / 100.0):
                (1.0 + totalPrimaryAttributes.getDexterity() / 100.0);
    }
}
