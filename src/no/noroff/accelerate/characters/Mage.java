package no.noroff.accelerate.characters;

import static no.noroff.accelerate.items.ArmorType.*;
import static no.noroff.accelerate.items.WeaponType.*;
import no.noroff.accelerate.items.ArmorType;
import no.noroff.accelerate.items.WeaponType;

public class Mage extends Character {

    // Constructor for mage
    public Mage(String name) {
        super(name, new PrimaryAttribute(1,1,8),
                new ItemType(new WeaponType[]{WAND, STAFF}, new ArmorType[]{CLOTH}));
    }

    // Method that increases level and base/total stats upon level up
    @Override
    public void levelUp() {
        level++;
        basePrimaryAttributes.increaseStrength(1);
        basePrimaryAttributes.increaseDexterity(1);
        basePrimaryAttributes.increaseIntelligence(5);
        totalPrimaryAttributes.increaseStrength(1);
        totalPrimaryAttributes.increaseDexterity(1);
        totalPrimaryAttributes.increaseIntelligence(5);
    }

    // Method that calculates DPS based on equipped weapon (if there is one) and total main stat
    @Override
    public double calculateDPS() {
        return weapon != null ? weapon.weaponDPS() * (1.0 + totalPrimaryAttributes.getIntelligence() / 100.0):
                (1.0 + totalPrimaryAttributes.getIntelligence() / 100.0);
    }
}
