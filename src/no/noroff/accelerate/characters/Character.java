package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.*;
import java.util.List;

public abstract class Character {
    // Character properties
    protected String name;
    protected int level = 1;
    protected PrimaryAttribute basePrimaryAttributes;
    protected PrimaryAttribute totalPrimaryAttributes;
    protected ItemType itemTypes;
    protected Weapon weapon;
    private Armor head;
    private Armor body;
    private Armor legs;

    // Constructor for character
    public Character(String name, PrimaryAttribute basePrimaryAttributes, ItemType itemTypes) {
        this.name = name;
        this.basePrimaryAttributes = basePrimaryAttributes;
        this.totalPrimaryAttributes = new PrimaryAttribute(basePrimaryAttributes.getStrength(),
                basePrimaryAttributes.getDexterity(), basePrimaryAttributes.getIntelligence());
        this.itemTypes = itemTypes;
    }

    // Abstract methods that are different for each of the character classes
    public abstract void levelUp();
    public abstract double calculateDPS();

    // Method that equips a weapon if the character class is able to
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        boolean equipped = false;
        List<WeaponType> weaponTypes = List.of(itemTypes.getWeaponTypes());

        // Checks for exception: level is too low or weapon can't be equipped by that character clas
        if (level < weapon.getRequiredLevel()) {
            throw new InvalidWeaponException("Your level is too low to equip this weapon");
        } else if (!weaponTypes.contains(weapon.getWeaponType())) {
            throw new InvalidWeaponException("The weapon can't be equipped by a " + this.getClass().getSimpleName());
        }

        // Equips the weapon and sets equipped to true for testing purposes
        this.weapon = weapon;
        equipped = true;

        return equipped;
    }

    // Method that equips an armor piece in the correct slot if the character class is able to
    public boolean equipArmor (Armor armor) throws InvalidArmorException {
        boolean equipped = false;
        List<ArmorType> armorTypes = List.of(itemTypes.getArmorTypes());

        // Checks for exception: level is too low or armor piece can't be equipped by that character clas
        if (level < armor.getRequiredLevel()) {
            throw new InvalidArmorException("Your level is too low to equip this armor piece");
        } else if (!armorTypes.contains(armor.getArmorType())) {
            throw new InvalidArmorException("The armor piece can't be equipped by a " + this.getClass().getSimpleName());
        }

        // Increases totalPrimaryAttributes when equipping an armor piece
        this.totalPrimaryAttributes.increaseStrength(armor.getArmorAttributes().getStrength());
        this.totalPrimaryAttributes.increaseDexterity(armor.getArmorAttributes().getDexterity());
        this.totalPrimaryAttributes.increaseIntelligence(armor.getArmorAttributes().getIntelligence());

        // Equips the armor piece in the correct slot and sets equipped to true for testing purposes
        if (armor.getSlot() == Slot.HEAD) {
            this.head = armor;
            equipped = true;
        } else if (armor.getSlot() == Slot.BODY) {
            this.body = armor;
            equipped = true;
        } else if (armor.getSlot() == Slot.LEGS) {
            this.legs = armor;
            equipped = true;
        }

        return equipped;
    }

    // Getters for character
    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public PrimaryAttribute getTotalPrimaryAttributes() {
        return totalPrimaryAttributes;
    }
}

