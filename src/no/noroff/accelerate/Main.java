package no.noroff.accelerate;

import no.noroff.accelerate.characters.*;
import no.noroff.accelerate.characters.Character;
import no.noroff.accelerate.items.*;
import no.noroff.accelerate.utils.ConsoleLogic;

public class Main {

    public static void main(String[] args) {
        // Starts the console application and asks the user for name and character class
        String name = ConsoleLogic.startCreation();
        String characterClass = ConsoleLogic.chooseCharacterClass(name);

        // Creates a character object based on the users choice
        Character character = switch (characterClass) {
            case "Mage" -> new Mage(name);
            case "Ranger" -> new Ranger(name);
            case "Rogue" -> new Rogue(name);
            case "Warrior" -> new Warrior(name);
            default -> null;
        };

        // Main console application loop - loops until the console is manually stopped
        while (true) {
            // Asks the user which action he wants to take
            String action = ConsoleLogic.chooseAction();

            // Levels up the character class if level up is chosen
            if (action.equals("level")) {
                int numberOfLevels = ConsoleLogic.levelUp(character);

                for (int i = 0; i < numberOfLevels; i++) {
                    character.levelUp();
                }
            }
            // Allows the user browse and select slot and itemtype to be equipped
            else if (action.equals("browse")) {
                Slot slot = ConsoleLogic.chooseSlot();
                if (slot == Slot.WEAPON) {
                    WeaponType weaponType = ConsoleLogic.chooseWeaponType();
                    // Made every weapon equal for simplicity's sake
                    Weapon weapon = new Weapon("Common " + weaponType.name(), 5, slot, weaponType, 5, 1.5);
                    // Tries to equip the weapon, invalid weapon throws a custom exception
                    try {
                        character.equipWeapon(weapon);
                    } catch (InvalidWeaponException e) {
                        System.out.println(e.getMessage());
                    }
                } else {
                    ArmorType armorType = ConsoleLogic.chooseArmorType();
                    // Made every armor piece equal for simplicity's sake
                    Armor armor = new Armor("Common " + armorType.name(), 5, slot, armorType, new PrimaryAttribute(2,2,2));
                    // Tries to equip the armor piece, invalid armor piece throws a custom exception
                    try {
                        character.equipArmor(armor);
                    } catch (InvalidArmorException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
            // Displays the character sheet using stringbuilder if this action is chosen
            else if (action.equals("sheet")) {
                StringBuilder sheet = new StringBuilder();
                sheet.append("Character name: ").append(character.getName());
                sheet.append("\nCharacter class: ").append(character.getClass().getSimpleName());
                sheet.append("\nCharacter level: ").append(character.getLevel());
                sheet.append("\nStrength: ").append(character.getTotalPrimaryAttributes().getStrength());
                sheet.append("\nDexterity: ").append(character.getTotalPrimaryAttributes().getDexterity());
                sheet.append("\nIntelligence: ").append(character.getTotalPrimaryAttributes().getIntelligence());
                sheet.append("\nDPS: ").append(character.calculateDPS());
                System.out.println(sheet);
            }
        }
    }
}
