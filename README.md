# RPG Character Creation

Console application in Java.

## Table of contents
1. [Background](#background)
2. [Installation](#installation)
3. [Usage](#usage)
4. [Author](#author)

## Background
This is a console application that uses the concepts covered in Module 1 of the Full Stack Java Course at Noroff Accelerate.

## Installation

* Install JDK 17
* Install IntelliJ IDEA
* Clone repository

## Usage

* Run the application through the main class and method

## Author
Jo Endre Brauten Urdal
